package com.example.roomwordssample;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WordDao {

    // This extended insert annotation manages conflicts that arise as a result of duplicate
    // word entries
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Word word);

    // Delete all words
    @Query("DELETE FROM word_table")
    void deleteAll();

    // Delete single word
    @Delete
    void deleteWord(Word word);

    @Query("SELECT * from word_table ORDER BY word ASC")
    LiveData<List<Word>> getAllWords();

    // For checking whether database is empty
    @Query("SELECT * from word_table LIMIT 1")
    Word[] getAnyWord();
}
