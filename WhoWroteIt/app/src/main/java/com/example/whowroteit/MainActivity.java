package com.example.whowroteit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mBookInput;
    private TextView mTitleText;
    private TextView mAuthorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBookInput = (EditText) findViewById(R.id.bookInput);
        mTitleText = (TextView) findViewById(R.id.titleText);
        mAuthorText = (TextView) findViewById(R.id.authorText);
    }

    public void searchBooks(View view) {
        // Get the search string from the input field.
        String queryString = mBookInput.getText().toString();


        // Hides keyboard when button is clicked.
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

        // Check whether the application has a network connection.
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connMgr != null) {
            networkInfo = connMgr.getActiveNetworkInfo();
        }


        // Check that a network connection exists, that the network is connected,
        // and that a query string is available.
        if (networkInfo != null && networkInfo.isConnected() && queryString.length() != 0) {
            // Launch the background task.
            new FetchBook(mTitleText, mAuthorText).execute(queryString);
        } else {
            // Handle query string being empty.
        if (queryString.length() == 0) {
            mAuthorText.setText("");
            mTitleText.setText(R.string.no_search_term);
            // Handle there being no network connection.
        } else {
            mAuthorText.setText("");
            mTitleText.setText(R.string.no_network);
        }
    }

        // After each new query, clear author TextView and set Title TextView to loading message.
        mAuthorText.setText("");
        mTitleText.setText(R.string.loading);
    }
}
